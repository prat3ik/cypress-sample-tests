# Cypress Example

An example project using the automation tool, [Cypress](https://www.cypress.io/).

I started porting my typical, _dorky_ tests over to using Cypress, as I've done with other automation tools. While writing the 4th test, I discovered that the tool [does not support new browser windows/tabs, nor iframes](https://docs.cypress.io/guides/references/trade-offs.html#Multiple-tabs). Sadly, this is a deal-breaker for me, but I had working code, so... here 'tis. 

## Running
In case you'd like to see it go!
1. clone it
2. `npm install`
3. `npm test`
4. click the test link in the Cypress app window
